angular.module('bengaliApp')
.controller('bengaliController', function($scope, $timeout, $stateParams, $http, $ionicSlideBoxDelegate, $ionicLoading, $sce) {
	$scope.autoAdvance = true;
	$scope.autoLoop = true;  
	var player;
	$scope.playbackRate = 1;
	$scope.words = [];
	$scope.deckId = $stateParams.deckId;
	$scope.paused = false;

	var $range = $("#playback").ionRangeSlider({
	    min: .5,
	    max: 3.0,
	    from: 1,
	    step: .02
	});

	$range.on("change", function () {
	    var $this = $(this),
	        value = $this.prop("value");
	    // set for next slide    
	    $scope.playbackRate = value;

	    // set for current slide
	    player.playbackRate = value;
	});

	$scope.options = {
		loop: true,
		effect: 'slide',
		initialSlide: 0
	}

	$http.get('words/' + $scope.deckId + '.json')
		.success(function(data) {
			// console.log(data);
			$scope.words = data;
		})
		.error(function(a,b,c,d,e){
			console.log(a,b,c,d,e);
			$scope.message = "Deck not available.";
		})

$scope.$on("$ionicSlides.sliderInitialized", function(event, data){
    // data.slider is the instance of Swiper
    // console.log("initialized");
    $scope.slider = data.slider;
    // console.log($scope.slider);
    // data.slider.params.loopAdditionalSlides = 2;
    data.slider.params.loop = true;
    // data.slider.slideTo(1);
});

$scope.$on("$ionicSlides.slideChangeStart", function(event, data){
     // console.log("change start");
     stopAudio($scope.activeIndex);
});

$scope.$on("$ionicSlides.slideChangeEnd", function(event, data){
    // note: the indexes are 0-based
    // console.log("changeEnd");
    $scope.activeIndex = data.slider.activeIndex;
    $scope.previousIndex = data.slider.previousIndex;
    playAudio($scope.activeIndex, data);
    // console.log("changeEnd - previous", data.slider.previousIndex);
});

$scope.trustSrc = function(src) {
	return       $sce.trustAsResourceUrl(src);
}

$scope.toggleSlideLoop = function () {
	$scope.slider.params.loop = !$scope.slider.params.loop;
}

$scope.togglePlay = function() {
	if ($scope.paused)
		player.play();
	else
		player.pause();

	$scope.paused = !$scope.paused;
}


//this is the function that helps decide which sound to play
function playAudio(number, data) {
	// console.log("playAudio number",number);
	var allPlayers = document.querySelectorAll(".audio");
	// allPlayers
	player = allPlayers[number];

	player.onended = function() {
		// console.log("play ended");
        // advance to next slide
        if ($scope.autoAdvance) {
        	$scope.slider.slideNext();
        }
    };
    player.playbackRate = $scope.playbackRate;
    player.play();
}

function stopAudio(number) {
	var allPlayers = document.querySelectorAll(".audio");
	var player = allPlayers[number];

	if (player != undefined) {
		player.pause();
		// player.currentTime = 0;
	}
}
})
  .directive('playbtn', function() {
    return {
      restrict: 'E',
      scope: {paused:'='},
      template: '<i ng-class="{\'ion-ios-play\': paused, \'ion-ios-pause\': !paused}"></i>'
    }
  });
